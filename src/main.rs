#[macro_use]
extern crate glium;

extern crate image;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate cgmath;


mod support;
mod traits;
mod objects;
mod rendering;
mod application;

use cgmath::Vector3;

fn main() {
    support::start_loop(|| {
        application::run()
    });
}

