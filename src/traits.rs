pub trait Render {
    fn render(&self);
}

pub trait Update {
    fn update(&mut self);
}

pub trait Pos {
    type Pos;
    fn pos(&self) -> &Self::Pos;
    fn set_pos(&mut self, &Self::Pos);
}

pub trait Vel {
    type Vel;
    fn vel(&self) -> &Self::Vel;
}


