use std;
use traits::*;
use cgmath::*;

struct Ball {
    pos: Vector3<f32>,
    vel: Vector3<f32>,
    rad: f32
}

//impl Update for Ball {}

impl Update for Ball {
    fn update(&mut self) {
        self.pos=self.pos+self.vel;
    }
}


struct Container {
    objects: std::vec::Vec<Ball>
}


