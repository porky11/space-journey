use glium;
use rendering;
use support;

use image;
use std::cell::RefCell;
use glium::Surface;
use glium::glutin;
use glium::index::PrimitiveType;
use std::io::Cursor;
use rendering::programs;

use glium::backend::glutin_backend::GlutinFacade as Display;


#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 3],
}
implement_vertex!(Vertex, position);

pub struct Data {
    pub display: glium::backend::glutin_backend::GlutinFacade,
    pub heightmap: glium::texture::Texture2d,
    pub vertex_buffer: glium::VertexBuffer<Vertex>,
    pub index_buffer: glium::IndexBuffer<u16>,
    pub planet_program: glium::Program,
    pub ball_program: glium::Program,
    pub tess_level: i32,
    pub time: f32
}

impl Data {
    pub fn new() -> Data {
        let display = {
            use glium::DisplayBuild;
            glium::glutin::WindowBuilder::new()
                .with_depth_buffer(24)
                .build_glium()
                .unwrap()
        };
        let ref display = display;

        let image = image::load(Cursor::new(&include_bytes!("Image.png")[..]),
                                image::PNG).unwrap().to_rgba();
        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
        let heightmap = glium::texture::Texture2d::new(display, image).unwrap();
        //let heightmap = glium::framebuffer::SimpleFrameBuffer::new(&display, cubemap.main_level().image(glium::texture::CubeLayer::PositiveX)).unwrap();

        let vertex_buffer = {

            glium::VertexBuffer::new(display, 
                &[
                    Vertex { position: [-1.0, -1.0, -1.0] },
                    Vertex { position: [-1.0, -1.0,  1.0] },
                    Vertex { position: [-1.0,  1.0, -1.0] },
                    Vertex { position: [-1.0,  1.0,  1.0] },
                    Vertex { position: [ 1.0, -1.0, -1.0] },
                    Vertex { position: [ 1.0, -1.0,  1.0] },
                    Vertex { position: [ 1.0,  1.0, -1.0] },
                    Vertex { position: [ 1.0,  1.0,  1.0] }
                 ]
            ).unwrap()
        };

        let index_buffer =
            glium::IndexBuffer::new(display,
                                    PrimitiveType::Patches { vertices_per_patch: 4 },
                                    &[0, 1, 3, 2,
                                      1, 0, 4, 5,
                                      0, 2, 6, 4,
                                      5, 4, 6, 7,
                                      2, 3, 7, 6,
                                      3, 1, 5, 7u16]).unwrap();

        let planet_program = glium::Program::new(display, programs::planet_program()).unwrap();
        let ball_program = glium::Program::new(display, programs::ball_program()).unwrap();

        let mut tess_level = 5i32;
        println!("The current tessellation level is {} ; use the Up and Down keys to change it", tess_level);
        
        // the main loop
        let mut time = 0.0f32;


        Data {
            heightmap: heightmap,
            vertex_buffer: vertex_buffer,
            index_buffer: index_buffer,
            planet_program: planet_program,
            ball_program: ball_program,
            tess_level: tess_level,
            time: time
        }
    }
    
    pub fn draw(&self, display: &Display) {

        //self.time += 0.01f32;
        let mut target = display.draw();

        target.clear_color_and_depth((0.0, 0.0, 0.0, 0.0),1.0);
        self.call_planet_program(&mut target);
        target.finish().unwrap();


        // polling and handling the events received by the window

    }

    pub fn call_planet_program(&self, target: &mut glium::Frame) {

        let vertex_buffer = &self.vertex_buffer;
        let index_buffer = &self.index_buffer;
        let program = &self.planet_program;
        let heightmap = &self.heightmap;
        let tess_level = self.tess_level;
        let time = self.time;



        let uniforms = uniform! {
            matrix: [
                [time.cos(), 0.0, time.sin(), 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [-time.sin(), 0.0, time.cos(), 0.0],
                [0.0, time.sin(), 0.0, 2.0f32]
            ],
            tess_level: tess_level,
            heightmap: heightmap,
            center: [0.0,0.0,0.0f32]
        };
        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            backface_culling: glium::draw_parameters::BackfaceCullingMode::CullCounterClockwise,
            .. Default::default()
        };
        target.draw(vertex_buffer, index_buffer, program, &uniforms, &params).unwrap();
    }
}

thread_local! {
    static DATA: RefCell<Data> = 
                 RefCell::new(Data::new());
}
    
pub fn run() -> support::Action {
    DATA.with(|data| {
        let data = data.borrow_mut();

        data.draw(data.display);
        
        for event in data.display.poll_events() {
            use glium::glutin::Event::*;
            match event {
                Closed => return support::Action::Stop,
                _ => ()
            }
        }
        support::Action::Continue
    })
}

