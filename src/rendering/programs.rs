use glium;

pub fn planet_program<'a>() -> glium::program::SourceCode<'a> {
    glium::program::SourceCode {
        vertex_shader: "
            #version 140

            in vec3 position;

            void main() {
                gl_Position = vec4(position, 1.0);
            }
        ",
        fragment_shader: "
            #version 140

            in vec2 tc;
            in vec3 color;
            out vec4 f_color;

            uniform sampler2D heightmap;

            void main() {
                vec3 col = texture(heightmap, tc).xyz;
                f_color = vec4(col, 1.0);
            }
        ",
        geometry_shader: Some("
            #version 330
            uniform mat4 matrix;
            layout(triangles) in;
            layout(triangle_strip, max_vertices=3) out;
            out vec3 color;
            out vec2 tc;
            
            uniform sampler2D heightmap;
            
            float rand(vec2 co) {
                return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
            }
            void main() {
                float g = 0.5;
                float g0 = g;
                float g1 = g;
                float g2 = g;
                //g0 = floor(abs(gl_in[0].gl_Position.z)+0.01);
                //g1 = floor(abs(gl_in[0].gl_Position.z)+0.01);
                //g2 = floor(abs(gl_in[0].gl_Position.z)+0.01);
                vec2 tc0 = (gl_in[0].gl_Position.xy+1)/2;
                vec2 tc1 = (gl_in[1].gl_Position.xy+1)/2;
                vec2 tc2 = (gl_in[2].gl_Position.xy+1)/2;
                float add0 = texture(heightmap, tc0).g*g0;
                float add1 = texture(heightmap, tc1).g*g1;
                float add2 = texture(heightmap, tc2).g*g2;
                vec3 all_color = vec3(
                    rand(gl_in[0].gl_Position.yz + gl_in[1].gl_Position.xz),
                    rand(gl_in[1].gl_Position.zx + gl_in[2].gl_Position.yx),
                    rand(gl_in[2].gl_Position.xy + gl_in[0].gl_Position.zy)
                );
                gl_Position = matrix * vec4(normalize(gl_in[0].gl_Position.xyz)*(1+add0),1);
                color = all_color;
                tc = tc0;
                EmitVertex();
                gl_Position = matrix * vec4(normalize(gl_in[1].gl_Position.xyz)*(1+add1),1);
                color = all_color;
                tc = tc1;
                EmitVertex();
                gl_Position = matrix * vec4(normalize(gl_in[2].gl_Position.xyz)*(1+add2),1);
                color = all_color;
                tc = tc2;
                EmitVertex();
            }
        "),
        tessellation_control_shader: Some("
            #version 410 core
             
            layout(vertices = 4) out;
            
            uniform int tess_level = 5;
            
            void main(void)
            {
             gl_TessLevelOuter[0] = tess_level;
             gl_TessLevelOuter[1] = tess_level;
             gl_TessLevelOuter[2] = tess_level;
             gl_TessLevelOuter[3] = tess_level;
             
             gl_TessLevelInner[0] = tess_level;
             gl_TessLevelInner[1] = tess_level;
             
             gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
            }
        "),
        tessellation_evaluation_shader: Some("
            #version 410 core
             
            layout(quads, equal_spacing, ccw) in;
             
            //quad interpol
            vec4 interpolate(in vec4 v0, in vec4 v1, in vec4 v2, in vec4 v3)
            {
             vec4 a = mix(v0, v1, gl_TessCoord.x);
             vec4 b = mix(v3, v2, gl_TessCoord.x);
             return mix(a, b, gl_TessCoord.y);
            }
             
            void main()
            { 
             vec4 pos = interpolate(
              gl_in[0].gl_Position, 
              gl_in[1].gl_Position, 
              gl_in[2].gl_Position, 
              gl_in[3].gl_Position);
              gl_Position = pos;
            }
        "),
    }
}

pub fn ball_program<'a>() -> glium::program::SourceCode<'a> {
    glium::program::SourceCode {
        vertex_shader: "
            #version 140

            in vec3 position;

            void main() {
                gl_Position = vec4(position, 1.0);
            }
        ",
        fragment_shader: "
            #version 140

            out vec4 f_color;

            uniform vec3 color;

            void main() {
                f_color = vec4(color, 1.0);
            }
        ",
        geometry_shader: Some("
            #version 330
            uniform mat4 matrix;
            layout(triangles) in;
            layout(triangle_strip, max_vertices=3) out;
            
            uniform sampler2D heightmap;
            
            float rand(vec2 co) {
                return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
            }
            void main() {
                gl_Position = matrix * vec4(normalize(gl_in[0].gl_Position.xyz),1);
                EmitVertex();
                gl_Position = matrix * vec4(normalize(gl_in[1].gl_Position.xyz),1);
                EmitVertex();
                gl_Position = matrix * vec4(normalize(gl_in[2].gl_Position.xyz),1);
                EmitVertex();
            }
        "),
        tessellation_control_shader: Some("
            #version 410 core
             
            layout(vertices = 4) out;
            
            uniform int tess_level = 5;
            
            void main(void)
            {
             gl_TessLevelOuter[0] = tess_level;
             gl_TessLevelOuter[1] = tess_level;
             gl_TessLevelOuter[2] = tess_level;
             gl_TessLevelOuter[3] = tess_level;
             
             gl_TessLevelInner[0] = tess_level;
             gl_TessLevelInner[1] = tess_level;
             
             gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
            }
        "),
        tessellation_evaluation_shader: Some("
            #version 410 core
             
            layout(quads, equal_spacing, ccw) in;
             
            //quad interpol
            vec4 interpolate(in vec4 v0, in vec4 v1, in vec4 v2, in vec4 v3)
            {
             vec4 a = mix(v0, v1, gl_TessCoord.x);
             vec4 b = mix(v3, v2, gl_TessCoord.x);
             return mix(a, b, gl_TessCoord.y);
            }
             
            void main()
            { 
             vec4 pos = interpolate(
              gl_in[0].gl_Position, 
              gl_in[1].gl_Position, 
              gl_in[2].gl_Position, 
              gl_in[3].gl_Position);
              gl_Position = pos;
            }
        "),
    }
}
